# Backup for Opticks package

Some changes are made to deal with some problems. These changes and the commit of this backup can be found in *MyChanges.log*.  
Note that if you don't want to visualise the geometry every time you run `OKX4Test` or `OTracerTest`, plese ignore the change in `ok/OKMgr.cc`.  

Some instructions about installing Opticks can be found in *installation*.  

An example of Opticks config file can be found in *.opticks_config*.   
