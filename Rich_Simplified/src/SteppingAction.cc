#include "SteppingAction.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4OpticksRecorder.hh"
#include "SSys.hh"

SteppingAction::SteppingAction(G4OpticksRecorder* okr_)
    :
    okr(okr_)
{
}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = SSys::getenvint(openvkey, 0);
    if(op_mode){
        okr->UserSteppingAction(step); 
    }
}



