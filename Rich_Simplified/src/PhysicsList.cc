#include "globals.hh"
#include "PhysicsList.hh"
#include "RichTbPhotoElectron.hh"
#include "RichTbMiscNames.hh"
#include "RichTbRunConfig.hh"
#include "Cerenkov.hh"
#include "L4Cerenkov.hh"
#include "CKMScintillation.hh"
#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpBoundaryProcess.hh"
#include "PmtPhotoElectricEffect.hh"
#include "PmtAnodeEnergyLoss.hh"
#include "PixelHpdPhotoElectricEffect.hh"
#include "HpdSiEnergyLoss.hh"

//#include "RichTbG4OpAbsorption.hh"
//#include "RichTbG4OpRayleigh.hh"
//#include "RichTbG4OpBoundaryProcess.hh"

#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ParticleTable.hh"
#include "G4VUserPhysicsList.hh"
#include "G4ParticleTable.hh"
#include "G4UserPhysicsListMessenger.hh"
#include "G4UImanager.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4UnitsTable.hh"
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4eMultipleScattering.hh"
#include "G4hMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#include "G4hIonisation.hh"
#include "G4Decay.hh"
#include "SArgs.hh"

//OBJECT CONSTRUCTOR/DESTRUCTOR
PhysicsList::PhysicsList(int argc, char** argv, const char* argforced):
	m_sargs(new SArgs(argc, argv, argforced)),
	G4VUserPhysicsList()
{
  defaultCutValue = 0.1 * CLHEP::mm;
  theParticleTable = G4ParticleTable::GetParticleTable(); 
  theParticleIterator = theParticleTable->GetIterator(); 
  //theMessenger = new G4UserPhysicsListMessenger(this);
} //constructor initialising pointers


PhysicsList::~PhysicsList(){
} 

//PARTICLE DEFINITION CONSTRUCTORS
void PhysicsList::ConstructParticle(){
  ConstructBosons();
  ConstructLeptons();
  ConstructMesons();
  ConstructBaryons();
}

void PhysicsList::ConstructBosons() {
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();
  G4Gamma::GammaDefinition();
  G4OpticalPhoton::OpticalPhotonDefinition();
}

void PhysicsList::ConstructLeptons() {
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();
  G4MuonPlus::MuonPlusDefinition();
  G4MuonMinus::MuonMinusDefinition();
  G4NeutrinoMu::NeutrinoMuDefinition();
  G4AntiNeutrinoMu::AntiNeutrinoMuDefinition();
  RichTbPhotoElectron::PhotoElectronDefinition();
}

void PhysicsList::ConstructMesons() {
  G4PionPlus::PionPlusDefinition();
  G4PionMinus::PionMinusDefinition();
  G4PionZero::PionZeroDefinition();
  G4KaonPlus::KaonPlusDefinition();
  G4KaonMinus::KaonMinusDefinition();
}

void PhysicsList::ConstructBaryons() {
  G4Proton::ProtonDefinition();
  G4AntiProton::AntiProtonDefinition();
  G4Neutron::NeutronDefinition();
  G4AntiNeutron::AntiNeutronDefinition();
}

//G4 PROCESS CONSTRUCTORS
void PhysicsList::ConstructProcess() {
  AddTransportation();
  ConstructGeneral();
  ConstructEM();
  ConstructOp();
}


void PhysicsList::ConstructGeneral() {
G4Decay * theDecayProcess = new G4Decay();

theParticleIterator->reset();
while ((*theParticleIterator)()) {
G4ParticleDefinition* particle = theParticleIterator->value();
G4ProcessManager* pmanager = particle->GetProcessManager();
  if (theDecayProcess->IsApplicable( * particle)) {
      pmanager->AddDiscreteProcess(theDecayProcess);
      pmanager->SetProcessOrdering(theDecayProcess, idxPostStep);
      pmanager->SetProcessOrdering(theDecayProcess, idxAtRest);
    }
  }
}

void PhysicsList::ConstructEM()
{
  G4cout << " Now creating EM processes" << G4endl;
  theParticleIterator->reset();
  while (( * theParticleIterator)()){
  G4ParticleDefinition * particle = theParticleIterator->value();
  G4ProcessManager * pmanager = particle->GetProcessManager();
  G4String particleName = particle->GetParticleName();
  if (particleName == "gamma") { //Construct processes for gamma
    pmanager->AddDiscreteProcess(new G4GammaConversion("conv"));
    pmanager->AddDiscreteProcess(new G4ComptonScattering("compt"));
    //pmanager->AddDiscreteProcess(new G4PhotoElectricEffect()); 
    //photo-electric disabled to avoid opticks hit confusions

    } else if (particleName == "e-") { //Construct processes for electron
    pmanager->AddProcess(new G4eMultipleScattering(), - 1, 1, 1);
    pmanager->AddProcess(new G4eIonisation(), - 1, 2, 2);
    pmanager->AddProcess(new G4eBremsstrahlung(), - 1, - 1, 3);

    } else if (particleName == "e+") { //Construct processes for positron
      pmanager->AddProcess(new G4eMultipleScattering(), - 1, 1, 1);
      pmanager->AddProcess(new G4eIonisation(), - 1, 2, 2);
      pmanager->AddProcess(new G4eBremsstrahlung(), - 1, - 1, 3);
      pmanager->AddProcess(new G4eplusAnnihilation("annihil"), 0, - 1, 4);

    } else if (particleName == "mu+" || particleName == "mu-") { // Construct processes for muon
      pmanager->AddProcess(new G4MuMultipleScattering(), - 1, 1, 1);
      pmanager->AddProcess(new G4MuIonisation(), - 1, 2, 2);
      pmanager->AddProcess(new G4MuBremsstrahlung(), - 1, - 1, 3);
      pmanager->AddProcess(new G4MuPairProduction(), - 1, - 1, 4);

    } else if (particle->GetPDGCharge() != 0.0 
        && particle->GetParticleName() != "chargedgeantino" 
        && particle-> GetParticleName() != "pe-"
        )
    { //all other charged particles besides geantino
      pmanager->AddProcess(new G4hMultipleScattering(), - 1, 1, 1);
      pmanager->AddProcess(new G4hIonisation(), - 1, 2, 2);
    }
  }
}




void PhysicsList::ConstructOp()
{
  RichTbRunConfig* rConfig = RichTbRunConfig::getRunConfigInstance();
  G4int aRadiatorConfiguration = rConfig->getRadiatorConfiguration();
  //Cerenkov* theCerenkovProcess = new Cerenkov();
  L4Cerenkov* theCerenkovProcess = new L4Cerenkov(m_sargs, "Cerenkov");
  //PmtAnodeEnergyLoss* thePmtAnodeEnergyLossProcess = new PmtAnodeEnergyLoss("PmtAnodeEnergyLoss");
  //HpdSiEnergyLoss* theHpdSiEnergyLossProcess = (aRadiatorConfiguration == 2) ? new HpdSiEnergyLoss("HpdSiEnergyLoss") : 0;
  
  CKMScintillation* theScintillationProcess = new CKMScintillation("Scintillation");

    //RichTbG4OpAbsorption* theAbsorptionProcess = new RichTbG4OpAbsorption();
    //RichTbG4OpRayleigh* theRayleighScatteringProcess = new RichTbG4OpRayleigh("RichTbG4OpRayleigh");
    //RichTbG4OpBoundaryProcess* theBoundaryProcess = new RichTbG4OpBoundaryProcess("RichTbG4BoundaryProcess");
    //PmtPhotoElectricEffect * thePmtPhotoElectricProcess = new PmtPhotoElectricEffect(PmtPhotElectProc);
    //PixelHpdPhotoElectricEffect * theHpdPhotoElectricProcess = (aRadiatorConfiguration == 2) ? new PixelHpdPhotoElectricEffect(HpdPhotElectProc) : 0;
    G4OpAbsorption* theAbsorptionProcess = new G4OpAbsorption();
    G4OpRayleigh* theRayleighScatteringProcess = new G4OpRayleigh();
    G4OpBoundaryProcess* theBoundaryProcess = new G4OpBoundaryProcess();

    theCerenkovProcess->SetMaxNumPhotonsPerStep(10); //reduced for testing
    theCerenkovProcess->SetTrackSecondariesFirst(true);
    theScintillationProcess->SetScintillationYieldFactor(1.);
    theScintillationProcess->SetTrackSecondariesFirst(true);

    //G4OpticalSurfaceModel themodel = glisur;
    //theBoundaryProcess->SetModel(themodel);

#if ( G4VERSION_NUMBER >= 1042 )
  auto particleIterator=GetParticleIterator();
  particleIterator->reset();
  while( (*particleIterator)() )
  {
      G4ParticleDefinition* particle = particleIterator->value();
#else
  theParticleIterator->reset();
  while( (*theParticleIterator)() )
  {
      G4ParticleDefinition* particle = theParticleIterator->value();
#endif
    G4ProcessManager* pmanager = particle->GetProcessManager();

    G4String particleName = particle->GetParticleName();

    if ( theCerenkovProcess && theCerenkovProcess->IsApplicable(*particle)) {
      pmanager->AddProcess(theCerenkovProcess);
      pmanager->SetProcessOrdering(theCerenkovProcess,idxPostStep);
    }

    //if (thePmtAnodeEnergyLossProcess->IsApplicable(*particle)){
    //  pmanager->AddProcess(thePmtAnodeEnergyLossProcess, -1, 2, 2);
    //}

    //if (theHpdSiEnergyLossProcess) {
    //  if (theHpdSiEnergyLossProcess->IsApplicable(*particle)){
    //    pmanager->AddProcess(theHpdSiEnergyLossProcess, - 1, 2, 2);
    //  }
    //}

    if ( theScintillationProcess && theScintillationProcess->IsApplicable(*particle)) {
      //pmanager->AddProcess(theScintillationProcess);
      //pmanager->SetProcessOrderingToLast(theScintillationProcess, idxAtRest);
      //pmanager->SetProcessOrderingToLast(theScintillationProcess, idxPostStep);
    }


    if (particleName == "opticalphoton") {
      pmanager->AddDiscreteProcess(theAbsorptionProcess);
      pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
      pmanager->AddDiscreteProcess(theBoundaryProcess);
    }
  }
}


void PhysicsList::SetCuts()
{
if (verboseLevel > 1) {
G4cout << "PhysicsList::SetCuts:";
}
//  " G4VUserPhysicsList::SetCutsWithDefault" method sets default cut value for all particle types
SetCutsWithDefault();
if (verboseLevel > 0) DumpCutValuesTable();
}
