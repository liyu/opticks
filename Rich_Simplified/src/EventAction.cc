#include <string>
#include <fstream>

#include "G4Event.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"
#include "G4HCtable.hh"

#include "EventAction.hh"
#include "SensitiveDetector.hh"
#include "OpHit.hh"
#include "RichPmtQuantEff.hh"
#include "RichTbMaterialParameters.hh"
#include "Randomize.hh"
#include "G4Opticks.hh"
#include "G4OpticksHit.hh"
#include "OpticksFlags.hh"
#include "U.hh"
#include "SArgs.hh"
#include "SSys.hh"

EventAction::EventAction(G4OpticksRecorder* okr_, SArgs* args_)
    :
    okr(okr_),
    m_sargs(args_)
{
}

void EventAction::BeginOfEventAction(const G4Event* anEvent)
{
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = SSys::getenvint(openvkey, 0);
    if(op_mode){
        okr->BeginOfEventAction(anEvent); 
    }
}

void EventAction::EndOfEventAction(const G4Event* event)
{
    //G4HCofThisEvent* HCE = event->GetHCofThisEvent() ;
    //assert(HCE); 

    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = U::getenvint(openvkey, 0);
    if(op_mode){
#ifdef EVENT_LOG 
   G4cout << "\n###[ EventAction::EndOfEventAction G4Opticks.propagateOpticalPhotons\n" << G4endl ; 
#endif
    G4Opticks* g4ok = G4Opticks::Get() ;
    G4int eventID = event->GetEventID() ; 
    unsigned num_hits = g4ok->propagateOpticalPhotons(eventID) ;  
#ifdef EVENT_LOG
    G4cout 
           << "EventAction::EndOfEventAction"
           << " eventID " << eventID
           << " num_hits " << num_hits 
           << G4endl 
           ; 
#endif
    G4OpticksHit hit ;
    G4OpticksHitExtra* hit_extra = NULL ;
    
    const char* envkey_substraction = "RichTbLHCbR1FlatMirror_mode" ;
    const char* envkey_exactflat = "RichTbLHCbR1ExactFlatMirror_mode" ;
    int substraction_mode = U::getenvint(envkey_substraction, 0) ;
    int exactflat_mode = U::getenvint(envkey_exactflat, 0) ;
    if( !m_sargs->hasArg("--nosave") ) {
        std::ofstream ofile;
        if( substraction_mode == 0 && exactflat_mode == 0 ) {
            ofile.open("Opticks_hits_"+std::to_string(eventID)+".txt"); } 
        if ( substraction_mode == 1 && exactflat_mode == 0 ) {
            ofile.open("Opticks_hits_subtraction_"+std::to_string(eventID)+".txt") ; }
        if ( substraction_mode == 0 && exactflat_mode == 1 ) {
            ofile.open("Opticks_hits_exactflat_"+std::to_string(eventID)+".txt") ; } 
        RichPmtQuantEff* quantEff = RichPmtQuantEff::getRichPmtQuantEffInstance();
        double RichTbLHCbCurrNominalPixelEff= PMTelectronicsDetAbsEff;
        double RichTbLHCbCurrQWARCoatingFactor = GasQuartzWindowARCoatingFactor;

        for(unsigned i=0 ; i < num_hits ; i++)
        {   
        g4ok->getHit(i, &hit, hit_extra ); 
        G4double particleKE = PhotWaveLengthToMom / (hit.wavelength * PmtQEWaveLenUnits);
        if( particleKE > 0.0 ) {
            G4double aQEVal = (G4double) ( quantEff->getPmtQEffFromPhotEnergy(particleKE));
            G4double randomnum = G4UniformRand();
            if( (aQEVal > 0.0) &&  (RichTbLHCbCurrNominalPixelEff > 0.0) && ( RichTbLHCbCurrQWARCoatingFactor > 0.0 ) && (randomnum <  ( aQEVal * RichTbLHCbCurrNominalPixelEff * RichTbLHCbCurrQWARCoatingFactor) ) ) {
                 std::cout 
            << std::setw(5) << i 
            << " boundary "           << std::setw(4) << hit.boundary 
            << " sensorIndex "        << std::setw(5) << hit.sensorIndex 
            << " nodeIndex "          << std::setw(5) << hit.nodeIndex 
            << " photonIndex "        << std::setw(5) << hit.photonIndex 
            << " flag_mask    "       << std::setw(10) << std::hex << hit.flag_mask  << std::dec
            << " sensor_identifier "  << std::setw(10) << std::hex << hit.sensor_identifier << std::dec
            << " wavelength "         << std::setw(8) << hit.wavelength 
            << " time "               << std::setw(8) << hit.time
            << " global_position "    << hit.global_position 
            << " " << OpticksFlags::FlagMask(hit.flag_mask, true)
            << std::endl 
            ;
                ofile << hit.global_position[0] << std::setw(10) << hit.global_position[1] << std::setw(10) <<
                hit.global_position[2] << std::endl;
            }
        }
        }
        ofile.close();
    }
    //g4ok->reset();  // necessary to prevent gensteps keeping to accumulate
    okr->EndOfEventAction(event);   // reset gets done in here 
#ifdef EVENT_LOG
    G4cout << "\n###] EventAction::EndOfEventAction G4Opticks.propagateOpticalPhotons\n" << G4endl ; 
#endif
    }
    //addDummyHits(HCE);
#ifdef EVENT_LOG
    G4cout 
         << "EventAction::EndOfEventAction"
         << " DumpHitCollections "
         << G4endl 
         ; 
#endif
    //SensitiveDetector::DumpHitCollections(HCE);

    // A possible alternative location to invoke the GPU propagation
    // and add hits in bulk to hit collections would be SensitiveDetector::EndOfEvent  
}

void EventAction::addDummyHits(G4HCofThisEvent* HCE)
{
    OpHitCollection* A = SensitiveDetector::GetHitCollection(HCE, "SD0/OpHitCollectionA");
    OpHitCollection* B = SensitiveDetector::GetHitCollection(HCE, "SD0/OpHitCollectionB");
    for(unsigned i=0 ; i < 10 ; i++) 
    {
        OpHitCollection* HC = i % 2 == 0 ? A : B ; 
        HC->insert( OpHit::MakeDummyHit() ) ;
    }
}



