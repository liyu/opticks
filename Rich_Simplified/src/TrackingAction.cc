#include "TrackingAction.hh"
#include "G4OpticksRecorder.hh"
#include "SSys.hh"

TrackingAction::TrackingAction(G4OpticksRecorder* okr_)
    :
    okr(okr_)
{
}

void TrackingAction::PreUserTrackingAction(const G4Track* track)
{
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = SSys::getenvint(openvkey, 0);
    if(op_mode){
        okr->PreUserTrackingAction(track); 
    }
}
void TrackingAction::PostUserTrackingAction(const G4Track* track)
{
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = SSys::getenvint(openvkey, 0);
    if(op_mode){
        okr->PostUserTrackingAction(track);     
    }
}


