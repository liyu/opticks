#include "RichPmtQuantEff.hh"
#include "RichTbMaterialParameters.hh"

RichPmtQuantEff * RichPmtQuantEff::RichPmtQuantEffInstance = 0;


RichPmtQuantEff::RichPmtQuantEff()
  : 
  m_PmtQeTable(std::map<double,double>() )
{
  RichPmtQuantEff::FillStdPmtQEInfo();
}


void RichPmtQuantEff::UpdatePmtQETable( double aEn,double aQE)  {
  m_PmtQeTable.insert({aEn,aQE});
}


double RichPmtQuantEff::getPmtQEffFromPhotEnergy(double aPhotonEnergy) {
  //if the energy is between 2 bins, interpolate.
  // assumes that energies are stored in increasing order in the class.
  // this means in the db they should be in the increasing order.
  //G4cout<< " RichPmtQuantEff photonE "<< photonenergy <<G4endl;

    typedef std::map<double,double>::const_iterator i_t;

    i_t i = m_PmtQeTable.upper_bound(aPhotonEnergy);

    if(i == m_PmtQeTable.end() ) return (--i)->second;

    if(i == m_PmtQeTable.begin() ) return i->second;
  
    i_t k = i;
    --k;

    const double delta = ((i->first) != (k->first)) ? ( aPhotonEnergy - (k->first) )/((i->first) - (k->first) ) : 0;

    return (delta * (i->second) + (1-delta)* (k->second)) ;
}


void RichPmtQuantEff::FillStdPmtQEInfo() {
  G4double PmtQEReductionFactor = 1.0;
  for(G4int iqb=0; iqb< PmtQENumBins ; iqb++){
    double CurQE = (double) ( LHCbTypePmtQE[iqb] * PmtQEReductionFactor );
    double CurEn = (double) ( PhotWaveLengthToMom/(PmtQEWaveLen[iqb]*PmtQEWaveLenUnits));
    UpdatePmtQETable( CurEn, CurQE );
  }
}


RichPmtQuantEff* RichPmtQuantEff::getRichPmtQuantEffInstance() {
  
  if(RichPmtQuantEffInstance == 0 ) 
    RichPmtQuantEffInstance  = new RichPmtQuantEff();

  return  RichPmtQuantEffInstance;
}
