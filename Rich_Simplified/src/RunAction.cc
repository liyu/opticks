#include <cassert>

#include "G4TransportationManager.hh"
#include "G4Opticks.hh"
#include <fstream>
#include "SSys.hh"
#include "RunAction.hh"

RunAction::RunAction(G4OpticksRecorder* okr_, SArgs* args_) 
    :   
    G4UserRunAction(),
    okr(okr_),
    m_sargs(args_)
{
}
void RunAction::BeginOfRunAction(const G4Run*)
{
    startTime = clock();
}
void RunAction::EndOfRunAction(const G4Run*)
{
    endTime = clock();
}

double RunAction::getTime()
{
    return (double)(endTime - startTime) / CLOCKS_PER_SEC;
}
