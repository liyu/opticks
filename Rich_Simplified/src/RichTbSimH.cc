#include "G4RunManager.hh"
#include "G4GeometryManager.hh"

#include "RichTbSimH.hh"

#include "RichTbOpticksDetectorConstruction.hh"
#include "RichTbMiscNames.hh"
#include "SensitiveDetector.hh"
#include "GDMLDetectorConstruction.hh"
//#include "RichTbPhysicsList.hh"
#include "PhysicsList.hh"
#include "L4Cerenkov.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4TransportationManager.hh"
#include "G4Opticks.hh"
#include "G4OpticksRecorder.hh"
#include "G4GDMLParser.hh"

//#include "Ctx.hh"
#include "PLOG.hh"
#include "SSys.hh"
#include "SArgs.hh"

#include "FTFP_BERT.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4OpticalParameters.hh"
#include "G4OpticalPhysics.hh"

#include "RunAction.hh"
#include "EventAction.hh"
#include "TrackingAction.hh"
#include "SteppingAction.hh"


RichTbSimH::RichTbSimH( int argc, char** argv, const char* argforced )
    :
    g4ok(new G4Opticks),
    okr(new G4OpticksRecorder),
    rm(new G4RunManager),
    //dc(new RichTbOpticksDetectorConstruction( argc, argv, argforced )),
    dc(nullptr),
    pl(new PhysicsList( argc, argv, argforced )),
    m_sargs( new SArgs(argc, argv, argforced) ),
    ga(nullptr),
    ra(nullptr),
    ea(nullptr),
    ta(nullptr),
    sa(nullptr)
{
    init();
}

void RichTbSimH::init()
{
    const char* gdmlpath = SSys::getenvvar("GDML_PATH",NULL);
    if(gdmlpath==NULL){
        dc = new RichTbOpticksDetectorConstruction( m_sargs ); 
        rm->SetUserInitialization(dc);
    }
    else {
        G4GDMLParser parser;
        //parser.SetOverlapCheck(true);
        parser.Read(gdmlpath);
        rm->SetUserInitialization( new GDMLDetectorConstruction(parser.GetWorldVolume()) );        
  
        //------------------------------------------------ 
        // Sensitive detectors
        //------------------------------------------------ 
        G4SDManager* SDman = G4SDManager::GetSDMpointer();    
        SensitiveDetector * PMTSD = new SensitiveDetector(PMTLHCbSDname, m_sargs);  
        SDman->AddNewDetector( PMTSD );
        ///////////////////////////////////////////////////////////////////////
        //
        // Example how to retrieve Auxiliary Information for sensitive detector
        //
        const G4GDMLAuxMapType* auxmap = parser.GetAuxMap();
        G4cout << "Found " << auxmap->size() << " volume(s) with auxiliary information." << G4endl << G4endl;
        for(G4GDMLAuxMapType::const_iterator iter=auxmap->begin(); iter!=auxmap->end(); iter++) 
        {
            G4cout << "Volume " << ((*iter).first)->GetName() << " has the following list of auxiliary information: " << G4endl << G4endl;
            for (G4GDMLAuxListType::const_iterator vit=(*iter).second.begin(); vit!=(*iter).second.end(); vit++)
            {
                G4cout << "--> Type: " << (*vit).type << " Value: " << (*vit).value << G4endl;
            }
        }
        G4cout << G4endl;
        //
        // The same as above, but now we are looking for
        // sensitive detectors setting them for the volumes
        //
        for(G4GDMLAuxMapType::const_iterator iter=auxmap->begin(); iter!=auxmap->end(); iter++) 
        {
            G4cout << "Volume " << ((*iter).first)->GetName() << " has the following list of auxiliary information: " << G4endl << G4endl;
            for (G4GDMLAuxListType::const_iterator vit=(*iter).second.begin(); vit!=(*iter).second.end();vit++)
            {
                if ((*vit).type=="SensDet")
                {
                    G4cout << "Attaching sensitive detector " << (*vit).value << " to volume " << ((*iter).first)->GetName() <<  G4endl << G4endl;
                    G4VSensitiveDetector* mydet = SDman->FindSensitiveDetector((*vit).value);
                    if(mydet) 
                    {
                        G4LogicalVolume* myvol = (*iter).first;
                        myvol->SetSensitiveDetector(mydet);
                    }
                } 
                else
                {
                    G4cout << (*vit).value << " detector not found" << G4endl;
                                                                                                                                                                                                              }
            }
        }
    }
    rm->SetUserInitialization(pl);
    //G4VModularPhysicsList* physicsList = new FTFP_BERT;
    //physicsList->ReplacePhysics(new G4EmStandardPhysics_option4());

    //G4OpticalPhysics* opticalPhysics = new G4OpticalPhysics();
    //auto opticalParams               = G4OpticalParameters::Instance();

    //opticalParams->SetWLSTimeProfile("delta");

    //opticalParams->SetScintYieldFactor(1.0);
    //opticalParams->SetScintExcitationRatio(0.0);
    //opticalParams->SetScintTrackSecondariesFirst(true);
    //opticalParams->SetScintEnhancedTimeConstants(true);

    //opticalParams->SetCerenkovMaxPhotonsPerStep(100);
    //opticalParams->SetCerenkovMaxBetaChange(10.0);
    //opticalParams->SetCerenkovTrackSecondariesFirst(true);

    //physicsList->RegisterPhysics(opticalPhysics);
    //rm->SetUserInitialization(physicsList);

    ga = new PrimaryGeneratorAction(m_sargs);
    ra = new RunAction(okr, m_sargs) ; 
    ea = new EventAction(okr, m_sargs) ; 
    ta = new TrackingAction(okr) ; 
    sa = new SteppingAction(okr) ; 

    rm->SetUserAction(ga);
    rm->SetUserAction(ra);
    rm->SetUserAction(ea);
    rm->SetUserAction(ta);
    rm->SetUserAction(sa);

    rm->Initialize(); 
    G4cout << " G4RunManager rm initialized successfully " << G4endl;
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = SSys::getenvint(openvkey, 0);
    if(op_mode){
    G4cout << "\n\n###[ G4Opticks.setGeometry\n\n" << G4endl ;
    G4VPhysicalVolume* world = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking()->GetWorldVolume() ;
    assert( world ) ;

    G4Opticks* g4ok = G4Opticks::Get();
    bool standardize_geant4_materials = false ;

    if( m_sargs->hasArg("--nosave") ) {
        const char* embedded_commandline_extra = "--skipaheadstep 1000 --cvd 0 --bouncemax 40 --recordmax 41 --rngmax 10 --nosave" ; // see ~/opticks/notes/issues/raja_repeated_photons.rst 
        g4ok->setEmbeddedCommandLineExtra(embedded_commandline_extra);
    }
    else {
        const char* embedded_commandline_extra = "--skipaheadstep 1000 --cvd 0 --bouncemax 40 --recordmax 41 --rngmax 10 " ; // see ~/opticks/notes/issues/raja_repeated_photons.rst 
        g4ok->setEmbeddedCommandLineExtra(embedded_commandline_extra);
    }
    if( m_sargs->hasArg("--usekey") ) {
        g4ok->loadGeometry();
        unsigned sensor_placements = g4ok->getNumSensorVolumes();
        for(unsigned i=0 ; i < sensor_placements  ; i++)
        {
            float efficiency_1 = 0.5f ;
            float efficiency_2 = 1.0f ;
            int sensor_cat = -1 ;                   // -1:means no angular efficiency info 
            int sensor_identifier = 0xc0ffee + i ;  // mockup a detector specific identifier
            unsigned sensorIndex = 1+i ;            // 1-based
            g4ok->setSensorData( sensorIndex, efficiency_1, efficiency_2, sensor_cat, sensor_identifier );
            //G4cout << sensorIndex << " " << sensor_identifier << G4endl;
        }
    }
    else {
        g4ok->setGeometry(world, standardize_geant4_materials );
        const std::vector<G4PVPlacement*>& sensor_placements = g4ok->getSensorPlacements() ;
        for(unsigned i=0 ; i < sensor_placements.size()  ; i++)
        {
            float efficiency_1 = 0.5f ;
            float efficiency_2 = 1.0f ;
            int sensor_cat = -1 ;                   // -1:means no angular efficiency info 
            int sensor_identifier = 0xc0ffee + i ;  // mockup a detector specific identifier
            unsigned sensorIndex = 1+i ;            // 1-based
            g4ok->setSensorData( sensorIndex, efficiency_1, efficiency_2, sensor_cat, sensor_identifier );
            //G4cout << sensorIndex << " " << sensor_identifier << G4endl;
        }
    }

    G4cout << "\n\n###] G4Opticks.setGeometry\n\n" << G4endl ;
    }
}


RichTbSimH::~RichTbSimH()
{
    G4GeometryManager::GetInstance()->OpenGeometry(); 
}

double RichTbSimH::beamOn(int num_ev)
{
    rm->BeamOn(num_ev); 
    return ra->getTime();
}

void RichTbSimH::Finalize()
{
    delete okr;
    delete rm;
}
