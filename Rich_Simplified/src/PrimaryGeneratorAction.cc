#include "PrimaryGeneratorAction.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleGun.hh"
#include "Randomize.hh"

#include "G4Opticks.hh"
#include "SArgs.hh"
#include <stdlib.h>
#include <string>
#include <sstream>
#include <fstream>

using CLHEP::MeV ; 

PrimaryGeneratorAction::PrimaryGeneratorAction(SArgs* args_)
    :
    G4VUserPrimaryGeneratorAction(),
    fParticleGun(new G4ParticleGun(1)),
    m_sargs(args_)
{
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition* particle;
    if( !m_sargs->hasArg("--particle") ){
        particle = particleTable->FindParticle("mu+");
    }
    else {
        const char* type = m_sargs->get_arg_after("--particle",NULL);
        const G4String particleName = G4String(type);
        particle = particleTable->FindParticle(particleName);
    }
    fParticleGun->SetParticleDefinition(particle);
    fParticleGun->SetParticleTime(0.0*CLHEP::ns);
    if( !m_sargs->hasArg("--position") ) {
        fParticleGun->SetParticlePosition(G4ThreeVector(0.0*CLHEP::cm,0.0*CLHEP::cm,0.0*CLHEP::cm));
    }
    else {
        const char* positions = m_sargs->get_arg_after("--position",NULL);
        std::string s = positions;
        int n = s.size();
        for(int i=0; i<n; i++) { if (s[i]==',') {s[i]=' ';} }
        std::istringstream ss(s);
        double pos[3]; std::string str; int i=0;
        while (ss >> str) {
            pos[i] = std::__cxx11::stod(str); i += 1;
            G4cout << str << G4endl; 
        }    
        G4cout << pos[0] << " " << pos[1] << " " << pos[2] << G4endl; 
        fParticleGun->SetParticlePosition(G4ThreeVector(pos[0]*CLHEP::cm,pos[1]*CLHEP::cm,pos[2]*CLHEP::cm));
    }
    if( !m_sargs->hasArg("--direction") ) {
        fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.1,1.));
    }
    else {
        const char* directions = m_sargs->get_arg_after("--direction",NULL);
        std::string s = directions;
        int n = s.size();
        for(int i=0; i<n; i++) { if (s[i]==',') {s[i]=' ';} }
        std::istringstream ss(s);
        double dir[3]; std::string str; int i=0;
        while (ss >> str) {
            dir[i] = std::__cxx11::stod(str); i += 1;
        }    
        G4cout << dir[0] << " " << dir[1] << " " << dir[2] << G4endl; 
        fParticleGun->SetParticleMomentumDirection( G4ThreeVector(dir[0],dir[1],dir[2]) );
    }
    if( !m_sargs->hasArg("--energy") ){
        fParticleGun->SetParticleEnergy(10000.*MeV);   // few photons at ~0.7*MeV loads from ~ 0.8*MeV
    }
    else {
        float energy = atof( m_sargs->get_arg_after("--energy",NULL) );
        fParticleGun->SetParticleEnergy(energy*MeV);
    }
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    std::ofstream ofile;
    ofile.open("primaryparticles.txt",std::ios::app);

    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition* particle;
    if( m_sargs->hasArg("--random") ){
        G4String particleName;
        G4double randNum = G4UniformRand();
        if( randNum < 0.25 )
            particleName = "mu+";
        else if( randNum>=0.25 && randNum<0.5 )
            particleName = "e+";
        else if( randNum>=0.5 && randNum<0.75 )
            particleName = "pi+";
        else
            particleName = "kaon+";
        particle = particleTable->FindParticle(particleName);
        ofile << std::setw(10) << particleName << std::setw(10);
    }
    if( m_sargs->hasArg("--random") ){
        G4double x = G4UniformRand()>0.5? -0.25*G4UniformRand():0.25*G4UniformRand();
        G4double y = 0.25*G4UniformRand();
        G4double z = 1.;
        fParticleGun->SetParticleMomentumDirection(G4ThreeVector(x,y,z));
        ofile << x << std::setw(10) << y << std::setw(10) << z << std::setw(10);
    }
    if( m_sargs->hasArg("--random") ){
        G4double energy = G4UniformRand()*10000.;
        fParticleGun->SetParticleEnergy( energy*MeV );
        ofile << energy << std::endl;
    }
    ofile.close(); 
    fParticleGun->GeneratePrimaryVertex(anEvent);

    // not strictly required by Opticks, but useful for debugging + visualization of non-opticals
    //G4Opticks::Get()->collectPrimaries(anEvent) ;  

}


 
