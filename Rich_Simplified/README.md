# Rich Simplified example
An example shows the interface of Opticks with Geant4, based on the Simplified RICH project (https://gitlab.cern.ch/seaso/richgpuopticks/-/tree/master/RichTbLHCbOpticks/cmtuser/RichTbSimH) and CerenkovMinimal example (https://bitbucket.org/simoncblyth/opticks/src/master/examples/Geant4/CerenkovMinimal/).


## How to use it?
`source build.sh` or `./build.sh clean`  
`cd build`  
`source ../GeometryTest.sh` Test the geometry in three cases: use the original option with deltatheta deltaphi, use the option of having an exact flat mirror, use the option with single delataTheta and box subtractions for test.  
`source ../TimeTest.sh` Compare the time consumed by hybrid workflow (Opticks+Geant4) with pure Geant4 workflow.  
`Rich_Simplified` Use the simplified RICH1 geometry.  


## Some useful options
`--usekey` Use *OPTICKS_KEY* to load the geometry.  
`--nosave` Don't save the information of events. Used for spee-up validations.  
`----noflatmirror` Construct the geometry without the flat mirror,which reflects the photons into the PMTs.  
`--numofevent` Followed by the number of events in a single run.  
`--particle` The type of input particle (e.g. mu+, e+, pi+, gamma, kaon+). By default mu+.  
`--energy` The energy of input particle in MeV. By default 10000.  
`--magnification` The magnification factor of the number of generated photons for a single event.  
`--position` The position of particle gun in the format of 0,0,0 (default, in cm).  
`--direction` The direction of particle momentum in the format of 0,0.1,1 (default).  
`--random` Generate events with random directions, particle types and initial energies.

## Some environment variables
`WITH_OPTICKS_mode` Running with Opticks simulation.  
`WITH_GEANT4_mode` Running with Geant4 simulation.  
`RichTbLHCbR1FlatMirror_mode` Use *G4IntersectionSolid* of *G4SubtractionSolid* (from two *G4Orb*) with a *G4Box* to construct the flat mirror.  
`RichTbLHCbR1ExactFlatMirror_mode` Use *G4Box* to model the flat mirror exactly.  If neither of them is true, the flat mirror is constructed from a *G4Sphere* cutted by theta and phi angle constraints.  
`RichTbLHCbR1SphMirror_mode`  Use *G4IntersectionSolid* of *G4SubtractionSolid* (from two *G4Orb*) with a *G4Box* to construct the spherical mirror. Otherwise, the spherical mirror is constructed from a *G4Sphere* cutted by theta and phi angle constraints.  
`GDML_PATH` Use provided gdmlpath to load the geometry.  
