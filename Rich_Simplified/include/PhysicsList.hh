#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "globals.hh"
#include "G4VUserPhysicsList.hh"
#include "G4ParticleTable.hh"
#include "SArgs.hh"

class PhysicsList:public G4VUserPhysicsList{
  public:
    PhysicsList(int argc, char** argv, const char* argforced); 
    virtual ~PhysicsList();
    SArgs* m_sargs;
  protected:
    
    virtual void SetCuts();

    // PARTICLE AND PROCESS CONSTRUCTORS
    virtual void ConstructGeneral();
    virtual void ConstructEM();
    virtual void ConstructOp();

    virtual void ConstructParticle();

    virtual void ConstructBosons();
    virtual void ConstructLeptons();
    virtual void ConstructMesons();
    virtual void ConstructBaryons();

    virtual void ConstructProcess();


  private:
    G4ParticleTable *theParticleTable; //List of existing particle types
    G4ParticleTable::G4PTblDicIterator *theParticleIterator;

};

#endif
