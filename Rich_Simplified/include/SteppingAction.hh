#pragma once

#include "G4UserSteppingAction.hh"
struct G4OpticksRecorder ; 

struct SteppingAction : public G4UserSteppingAction
{
    SteppingAction(G4OpticksRecorder* okr_); 
    virtual void UserSteppingAction(const G4Step* step);

    G4OpticksRecorder* okr ; 
};




