#pragma once

#include "G4UserEventAction.hh"
#include "G4OpticksRecorder.hh"
#include "SArgs.hh"
struct G4OpticksRecorder ; 

struct EventAction : public G4UserEventAction
{
    EventAction(G4OpticksRecorder* okr, SArgs* args_); 
    virtual void BeginOfEventAction(const G4Event* anEvent);
    virtual void EndOfEventAction(const G4Event* anEvent);

    void addDummyHits(G4HCofThisEvent* HCE);

    G4OpticksRecorder*  okr ; 
    SArgs* m_sargs ;
};
