#pragma once

#include "G4UserRunAction.hh"
#include <ctime>
#include "G4OpticksRecorder.hh"
#include "SArgs.hh"

struct G4OpticksRecorder ; 

struct RunAction : public G4UserRunAction
{
    RunAction(G4OpticksRecorder* okr, SArgs* args_); 
    virtual void BeginOfRunAction(const G4Run* run);
    virtual void EndOfRunAction(const G4Run* run);
    G4OpticksRecorder*       okr ; 
    SArgs* m_sargs ; 
    clock_t startTime, endTime;
    double getTime();
};
