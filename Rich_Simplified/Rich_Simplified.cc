#include "OPTICKS_LOG.hh"
#include "RichTbSimH.hh"
#include "SArgs.hh"
#include <stdlib.h>

int main( int argc, char** argv, const char* argforced )
{
    OPTICKS_LOG( argc, argv );

    RichTbSimH rich( argc, argv, argforced );
    
    SArgs* m_sargs = new SArgs( argc, argv, argforced );
    const char* numofevent = m_sargs->get_arg_after("--numofevent", NULL);

    if( numofevent==NULL ) {
        double time = rich.beamOn(1);
    }
    else {
        int events = int( atof(numofevent) );       
        double time = rich.beamOn(events);
    }
    return 0;
}
