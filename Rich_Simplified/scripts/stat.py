from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from opticks.ana.hismask import HisMask
from scipy.stats import norm

hm = HisMask()

num_hits = {}; num_hits["g4"] = []; num_hits["op"] = []
mu = {"g4":0, "op":0}
std = {"g4":0, "op":0}
for i in range(1, 501):
    ox_op_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/'+str(i)+'/ox.npy')
    ox_g4_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/-'+str(i)+'/ox.npy')

    op_result = []; g4_result = []
    op_photons = []; g4_photons = []

    for j in range(len(ox_op_result[:,3].view(np.int32))):
        if( "SD" in hm.label(ox_op_result[j,3,3].view(np.int32)) ):
            op_result.append( [ox_op_result[j,0,:],ox_op_result[j,1,:]] )
        op_photons.append( [ox_op_result[j,0,:],ox_op_result[j,1,:]] )
    for k in range(len(ox_g4_result[:,3].view(np.int32))):
        if( "SD" in hm.label(ox_g4_result[k,3,3].view(np.int32)) ):
            g4_result.append( [ox_g4_result[k,0,:],ox_g4_result[k,1,:]] )
        g4_photons.append( [ox_g4_result[k,0,:],ox_g4_result[k,1,:]] )
    num_hits["g4"].append( len(g4_result) )
    num_hits["op"].append( len(op_result) )

num_hits["g4"] = np.array(num_hits["g4"])
num_hits["op"] = np.array(num_hits["op"])
plt.hist( num_hits["g4"], bins=20, color="blue", density=True )
plt.hist( num_hits["op"], bins=20, color="red", density=True )

mu["g4"], std["g4"] = norm.fit( num_hits["g4"] )
mu["op"], std["op"] = norm.fit( num_hits["op"] )
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
g4 = norm.pdf(x, mu["g4"], std["g4"])
plt.plot(x, g4, 'b-', linewidth=2, label="GEANT4: $\mu={:.2f},\sigma={:.2f}$".format( mu["g4"], std["g4"] ) )
op = norm.pdf(x, mu["op"], std["op"])
plt.plot(x, op, 'r-', linewidth=2, label="Opticks: $\mu={:.2f},\sigma={:.2f}$".format( mu["op"], std["op"] ) )

plt.legend(loc='best')
plt.title("500 events")
plt.xlabel("number of hits")
plt.ylabel("")
plt.savefig("figures/stat_500events_hits.pdf")
plt.show()
plt.close()


R = {}; R["g4"] = []; R["op"] = []
mu = {"g4":0, "op":0}
std = {"g4":0, "op":0}
for i in range(1, 501):
    ox_op_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/'+str(i)+'/ox.npy')
    ox_g4_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/-'+str(i)+'/ox.npy')

    op_result = []; g4_result = []

    for j in range(len(ox_op_result[:,3].view(np.int32))):
        if( "SD" in hm.label(ox_op_result[j,3,3].view(np.int32)) and abs(ox_op_result[j,0,0])<150  ):
            op_result.append( ox_op_result[j,0,:] )
    for k in range(len(ox_g4_result[:,3].view(np.int32))):
        if( "SD" in hm.label(ox_g4_result[k,3,3].view(np.int32)) and abs(ox_op_result[k,0,0])<150 ):
            g4_result.append( ox_g4_result[k,0,:] )
    op_photons = np.array(op_result); g4_photons = np.array(g4_result)
    pos_op = [ 0., np.mean(op_photons[:,1]), np.mean(op_photons[:,2])  ]
    pos_g4 = [ 0., np.mean(g4_photons[:,1]), np.mean(g4_photons[:,2])  ]
    print( pos_op, pos_g4 )
    radius = 0
    for j in range( len(op_result) ):
        radius += sqrt( pow(op_photons[j,0]-pos_op[0],2) + pow(op_photons[j,1]-pos_op[1],2) + pow(op_photons[j,2]-pos_op[2],2) )        
    R["op"].append( radius/len(op_result) )
    radius = 0
    for j in range( len(g4_result) ):
        radius += sqrt( pow(g4_photons[j,0]-pos_g4[0],2) + pow(g4_photons[j,1]-pos_g4[1],2) + pow(g4_photons[j,2]-pos_g4[2],2) )        
    R["g4"].append( radius/len(g4_result) )

R["g4"] = np.array(R["g4"])
R["op"] = np.array(R["op"])
bins = np.linspace(91, 93, 20)
plt.hist( R["g4"], bins, color="blue", histtype="step" )
plt.hist( R["op"], bins, color="red", histtype="step" )

mu["g4"], std["g4"] = norm.fit( R["g4"] )
mu["op"], std["op"] = norm.fit( R["op"] )
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
g4 = norm.pdf(x, mu["g4"], std["g4"])
plt.plot(x, g4, 'b-', linewidth=2, label="GEANT4: $\mu={:.2f},\sigma={:.2f}$".format( mu["g4"], std["g4"] ) )
op = norm.pdf(x, mu["op"], std["op"])
plt.plot(x, op, 'r-', linewidth=2, label="Opticks: $\mu={:.2f},\sigma={:.2f}$".format( mu["op"], std["op"] ) )

plt.legend(loc='best')
plt.title("500 events")
plt.xlabel("Radius")
plt.ylabel("")
#plt.savefig("figures/stat_500events_hits.pdf")
plt.show()
plt.close()
