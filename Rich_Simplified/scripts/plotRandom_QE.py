from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from opticks.ana.hismask import HisMask
import os

input_file = open("primaryparticles.txt","r")
primary_particle = []
for line in input_file:
    temp = [s for s in line.strip().split()]
    if( temp[0]=="mu+" ):
        temp.append( "o" )
    elif( temp[0]=="e+" ):
        temp.append( "+" )
    elif( temp[0]=="pi+" ):
        temp.append( "*" )
    else:
        temp.append( "s" )
    primary_particle.append(temp)


for e in range(1,21):
    if not os.path.isfile( 'Opticks_hits_subtraction_'+str(e)+'.txt' ):
        continue

    op_file = open("Opticks_hits_subtraction_"+str(e)+".txt","r")
    op_result = []
    for line in op_file:
        temp = [float(s) for s in line.strip().split()]
        if( temp[1]>0 ):
            op_result.append(temp);
    op_result = np.array(op_result)

    g4_file = open("Geant4_hits_"+str(e)+".txt","r")
    g4_result = []
    for line in g4_file:
        temp = [float(s) for s in line.strip().split()]
        if( temp[1]>0 ):
            g4_result.append(temp)
    g4_result = np.array(g4_result)

    if( op_result.shape[0]>0 ):
        plt.scatter(op_result[:,0],op_result[:,1],s=10, color='red', alpha=0.5, marker='D' ) #,marker=primary_particle[e-1][5],label=primary_particle[e-1][0])
    if( g4_result.shape[0]>0 ):
        plt.scatter(g4_result[:,0],g4_result[:,1],s=10,color='blue', alpha=0.2 )#,marker=primary_particle[e-1][5],label=primary_particle[e-1][0])

plt.legend(loc='best')
plt.title("Hits collected by Opticks (in red) and Geant4 (in blue)")
plt.xlabel("x/mm")
plt.ylabel("y/mm")
plt.savefig("draw_random_Hits_xy.pdf")
plt.show()
plt.close()


for e in range(1,21):
    if not os.path.isfile( 'Opticks_hits_subtraction_'+str(e)+'.txt' ):
        continue

    op_file = open("Opticks_hits_subtraction_"+str(e)+".txt","r")
    op_result = []
    for line in op_file:
        temp = [float(s) for s in line.strip().split()]
        if( temp[1]>0 ):
            op_result.append(temp);
    op_result = np.array(op_result)

    g4_file = open("Geant4_hits_"+str(e)+".txt","r")
    g4_result = []
    for line in g4_file:
        temp = [float(s) for s in line.strip().split()]
        if( temp[1]>0 ):
            g4_result.append(temp)
    g4_result = np.array(g4_result)

    if( op_result.shape[0]>0 ):
        plt.scatter(op_result[:,1],op_result[:,2],s=10, color='red', alpha=0.5, marker='D' ) #,marker=primary_particle[e-1][5],label=primary_particle[e-1][0])
    if( g4_result.shape[0]>0 ):
        plt.scatter(g4_result[:,1],g4_result[:,2],s=10,color='blue', alpha=0.2 )#,marker=primary_particle[e-1][5],label=primary_particle[e-1][0])

plt.legend(loc='best')
plt.title("Hits collected by Opticks (in red) and Geant4 (in blue)")
plt.xlabel("y/mm")
plt.ylabel("z/mm")
plt.savefig("draw_random_Hits_yz.pdf")
plt.show()
plt.close()


