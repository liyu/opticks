import numpy as np
from opticks.ana.hismask import HisMask

path = '/tmp/liyu/opticks/source/evt/g4live/natural/'

op_ox = np.load(path+'1/ox.npy').view(np.int32)
g4_ox = np.load(path+'-1/ox.npy').view(np.int32)

results = {}; 

for op in op_ox.view(np.int32)[:,3]:
	if( op[3] not in results ):
		results[op[3]] = [1,0]
	else:
		results[op[3]][0] += 1
for g4 in g4_ox.view(np.int32)[:,3]:
	if( g4[3] in results ):
		results[g4[3]][1] += 1

hm = HisMask()
print("{0:<20}\t{1:<20} \t{2:<20} \t {3:<20} ".format("hismsk","label","Opticks(1)","Geant4(-1)"))
for key in sorted(results):
	print( "{0:<20}\t{1:<20}\t{2:<20}\t{3:<20}".format(key, hm.label(key), results[key][0], results[key][1]) )

