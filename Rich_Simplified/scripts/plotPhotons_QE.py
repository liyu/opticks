from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from opticks.ana.hismask import HisMask

op_file = open("build/Opticks_hits_subtraction_0.txt","r")
op_result = []
for line in op_file:
    temp = [float(s) for s in line.strip().split()]
    op_result.append(temp);
op_result = np.array(op_result)

g4_file = open("build/Geant4_hits_0.txt","r")
g4_result = []
for line in g4_file:
    temp = [float(s) for s in line.strip().split()]
    g4_result.append(temp)
g4_result = np.array(g4_result)


plt.scatter(op_result[:,0],op_result[:,1],s=20,color='red', alpha=0.5, marker='D', label='Opticks: '+str(op_result.shape[0])+' hits')
plt.scatter(g4_result[:,0],g4_result[:,1],s=20,color='blue', alpha=0.5, label='Geant4: '+str(g4_result.shape[0])+' hits')
plt.legend(loc='best')
plt.title("Hits collected by Opticks and Geant4")
plt.xlabel("x/mm")
plt.ylabel("y/mm")
plt.savefig("figures/draw_Hits_xy_QE.pdf")
plt.show()
plt.close()

plt.scatter(op_result[:,1],op_result[:,2],s=20,color='red', alpha=0.5, marker='D', label='Opticks: '+str(op_result.shape[0])+' hits')
plt.scatter(g4_result[:,1],g4_result[:,2],s=20,color='blue', alpha=0.5, label='Geant4: '+str(g4_result.shape[0])+' hits')
plt.legend(loc='best')
plt.title("Hits collected by Opticks and Geant4")
plt.xlabel("y/mm")
plt.ylabel("z/mm")
plt.savefig("figures/draw_Hits_yz_QE.pdf")
plt.show()
plt.close()

