from math import sqrt
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import norm
name = sys.argv[1]


num_hits = {}; num_hits["g4"] = []; num_hits["op"] = []
mu = {"g4":0, "op":0}
std = {"g4":0, "op":0}
for i in range(0, 500):
    op_file = open("build/Opticks_hits_subtraction_"+str(i)+".txt","r")
    op_result = []
    for line in op_file:
        temp = [float(s) for s in line.strip().split()]
        op_result.append(temp);
    op_result = np.array(op_result)

    g4_file = open("build/Geant4_hits_"+str(i)+".txt","r")
    g4_result = []
    for line in g4_file:
        temp = [float(s) for s in line.strip().split()]
        g4_result.append(temp)
    g4_result = np.array(g4_result)

    num_hits["g4"].append( len(g4_result) )
    num_hits["op"].append( len(op_result) )

num_hits["g4"] = np.array(num_hits["g4"])
num_hits["op"] = np.array(num_hits["op"])
plt.hist( num_hits["g4"], bins=20, color="blue", density=True, alpha=0.5 )
plt.hist( num_hits["op"], bins=20, color="red", density=True, alpha=0.5 )

mu["g4"], std["g4"] = norm.fit( num_hits["g4"] )
mu["op"], std["op"] = norm.fit( num_hits["op"] )
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
g4 = norm.pdf(x, mu["g4"], std["g4"])
plt.plot(x, g4, 'b-', linewidth=2, label="GEANT4: $\mu={:.2f},\sigma={:.2f}$".format( mu["g4"], std["g4"] ) )
op = norm.pdf(x, mu["op"], std["op"])
plt.plot(x, op, 'r-', linewidth=2, label="Opticks: $\mu={:.2f},\sigma={:.2f}$".format( mu["op"], std["op"] ) )

plt.legend(loc='best')
plt.title("500 "+name+"s")
plt.xlabel("number of hits")
plt.ylabel("")
plt.savefig("figures/stat_500"+name+"s_hits_QE.pdf")
plt.show()
plt.close()

