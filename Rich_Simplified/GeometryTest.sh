#!/bin/bash -l 

export X4PhysicalVolume=INFO
export GMeshLib=INFO
#export WITH_OPTICKS_mode=1
#export WITH_GEANT4_mode=1
#default: use the original option with deltatheta deltaphi
export RichTbLHCbR1ExactFlatMirror_mode=0
export RichTbLHCbR1SphMirror_mode=0
export RichTbLHCbR1FlatMirror_mode=0 
if [ -n "$DEBUG" ]; then 
    lldb__ Rich_Simplified
else
    Rich_Simplified
fi  
#From Sajan: use the option of having an exact flat mirror
export RichTbLHCbR1ExactFlatMirror_mode=1
export RichTbLHCbR1SphMirror_mode=0
export RichTbLHCbR1FlatMirror_mode=0 
if [ -n "$DEBUG" ]; then 
    lldb__ Rich_Simplified
else
    Rich_Simplified
fi  

#From Simon: use the option with single deltaTheta and box substractions for test
export RichTbLHCbR1ExactFlatMirror_mode=0
export RichTbLHCbR1SphMirror_mode=1
export RichTbLHCbR1FlatMirror_mode=1 
if [ -n "$DEBUG" ]; then 
    lldb__ Rich_Simplified
else
    Rich_Simplified
fi  

python ../scripts/draw_GeometryTest.py

export RichTbLHCbR1SphMirror_mode=1
export RichTbLHCbR1FlatMirror_mode=1 
