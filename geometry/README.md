# Geometry files related to this project
`rich1_new.gdml`: RICH1 geometry with the VELO vessel and beam support structure.  
`RICH1.gdml`: RICH1 geometry.  
`RICH1_noR1MapmtMTBorderSurface.gdml`: RICH1 geometry, with the bordersurfaces in PMT envelope structure removed.  
`LXe.gdml`: A box filled with liquid xenon.  
`RichTb.gdml`: RICH test beam geometry.  
`Rich_Simplified.gdml`: Simplified RICH1 geometry.  

## How to obtain the RICH1 geometry
1. From Gauss: see `Gauss-Job.py`. 
2. From DD4Hep: see https://gitlab.cern.ch/lhcb/Detector/-/blob/master/doc/HOWTO.md.
```
gGeoManager->Export("RICH1.root","RICH1") && .q
source /cvmfs/sft.cern.ch/lcg/views/LCG_103/x86_64-centos7-gcc11-dbg/setup.sh
export PYTHONPATH=$ROOTSYS/lib:$ROOTSYS/geom/gdml
root -l RICH1.root
RICH1->Export("RICH1.gdml")
```
The reason is that the DD4Hep use the TGeoManager class in ROOT to store and export the geometry. But when you export the geometry into a gdml file, you need to include the correct python scripts (you can find them in $ROOTSYS/geom/gdml), as explained here: https://root.cern.ch/doc/master/TGeoManager_8cxx_source.html#l03713  
Note that to remove the bordersurfaces of PMT envelopes, this line in [Detector/Rich1/src/Rich1_geo.cpp](https://gitlab.cern.ch/lhcb/Detector/-/blob/master/Detector/Rich1/src/Rich1_geo.cpp#L900) is commented out.

## How to visualise the geometry with Opticks
`OKX4Test --deletegeocache --gdmlpath ~/opticks/geometry/rich1_new.gdml --x4balanceskip 74,90,94 --x4nudgeskip 857,867 --x4pointskip 74,867`   
`cd CSG_GGeo` or `cg`      
Add `export X4Solid_convertSphere_enable_phi_segment=1` to .opticks_config   
`./run.sh`   
`cd CSGOptiX`   
`opticks-build7` or `b7`   
`EYE=0.5,0,0.5 MOI=Rich1MasterWithSubtract0xcf514d0 ./cxr_view.sh`   
`MOI=Rich1MasterWithSubtract_shape_0x42d1ab0 EYE=-0.4,0,0 ZOOM=2 TMIN=0.4 CAM=1 LOOK=1,0.1,0.1 ./cxr_view.sh`
